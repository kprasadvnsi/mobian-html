Thanks to :

Initial site template :
https://html5up.net/photon

After CSS mods unused have been removed with the help of :
https://purifycss.online/

As the site above minified re-format was made with :
https://www.cleancss.com/

For html the formatted code was verified with :
https://www.freeformatter.com/

Terms of service and policies was generated with :
https://www.termsfeed.com/
https://www.privacypolicies.com/

Markdown to HTML transcode with css support was made with :
https://markdowntohtml.com/
